#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class congrats():
    def __init__(self, nombre="", apellido=""):
        text = "Se agrego el siguiente integrante: "
        espacio = " "
        """
            como solo se acepta un item en secondary_text todo los strings
            se unen en texto completo para usarlo
        """
        textcompleto = text + nombre + espacio + apellido

        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.congrats = self.builder.get_object("wnSuccess")
        self.congrats.set_property("text",
                                   "¡Felicidades!")
        self.congrats.set_property("secondary_text",
                                   textcompleto)
