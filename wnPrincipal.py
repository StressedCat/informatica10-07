#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

from dlgPrincipal import dlgPrincipal

from Felicidades import congrats

class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Ejemplo")
        self.window.maximize()
        self.window.show_all()

        # botones de menú
        boton_aceptar = self.builder.get_object("btnAnadir")
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)
        boton_editar = self.builder.get_object("btnEditar")
        boton_editar.connect("clicked", self.boton_editar_clicked)
        boton_eliminar = self.builder.get_object("btnEliminar")
        boton_eliminar.connect("clicked", self.boton_eliminar_clicked)

        # label
        self.label_changed = self.builder.get_object("labelChanged")

        # liststore
        self.liststore = self.builder.get_object("treeEjemplo")
        self.liststore.connect("cursor-changed", self.liststore_changed)
        # self.model = Gtk.ListStore(str, str)
        self.model = Gtk.ListStore(*(2 * [str]))
        self.liststore.set_model(model=self.model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Nombre",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)

        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        # cell_edit.set_property("background", "red")
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        # cell_edit.set_property("background", "red")
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        self.liststore.append_column(column)

        self.model.append(["Fabio", "Durán"])
        self.model.append(["Pepito", "Pepón"])

    def liststore_changed(self, tree=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        nombre = model.get_value(it, 0)
        nombre = ''.join(["Nombre: ",
                          nombre])
        apellido = model.get_value(it, 1)
        self.label_changed.set_text(nombre + apellido)

    def cell_text_apellido_edited(self, cell_renderer, path, text):
        self.model[path][1] = text

    def boton_aceptar_clicked(self, btn=None):
        dlg = dlgPrincipal(titulo="Añadir persona")
        response = dlg.dialogo.run()

        if response == Gtk.ResponseType.OK:
            nombre = dlg.nombre.get_text()
            apellido = dlg.apellido.get_text()
            """
                si el nombre o apellido estan vacios no se realizará la accion
                de añadir a alguien a la lista, por lo tanto no se agregara
                un espacio vacio, en el caso contrario se agregará el
                integrante a la lista junto con con avisar que la accion fue
                hecha
            """
            if nombre != "" and apellido != "":
                self.model.append([nombre, apellido])
                popout = congrats(nombre=nombre, apellido=apellido)
                answer = popout.congrats.run()
                if answer == Gtk.ResponseType.OK:
                    popout.congrats.destroy()
            dlg.dialogo.destroy()

    def boton_editar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        dlg = dlgPrincipal()
        dlg.nombre.set_text(model.get_value(it, 0))
        dlg.apellido.set_text(model.get_value(it, 1))

        response = dlg.dialogo.run()
        if response == Gtk.ResponseType.OK:
            model.set_value(it, 0, dlg.nombre.get_text())
            model.set_value(it, 1, dlg.apellido.get_text())
            dlg.dialogo.destroy()

    def boton_eliminar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return
        else:
            text = "se borrara el siguiente nombre: "
            nombre = (model.get_value(it, 0))
            apellido = (model.get_value(it, 1))
            espacio = " "
            """
                como solo se acepta un item en secondary_text todo los strings
                se unen en texto completo para usarlo
            """
            textocompleto = text + nombre + espacio + apellido

            borrar = self.builder.get_object("wnDelete")
            borrar.set_property("text",
                                "Advertencia")
            borrar.set_property("secondary_text",
                                textocompleto)

            answer = borrar.run()
            if answer == Gtk.ResponseType.YES:
                model.remove(it)
            # en el caso que sea no, se realizará nada
            else:
                pass
            borrar.hide()


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()
